================
Example Tutorial
================

Idem plugin to get information about the TLS certificates that protects a URL. TLS certificate information(e.g. SHA1 fingerprint) can be used with other idem plugin e.g. idem-aws when creating resources.
