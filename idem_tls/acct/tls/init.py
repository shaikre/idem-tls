async def gather(hub, profiles):
    """
    load tls profiles from credential files

    Example:
    .. code-block:: yaml

        tls:
          default:
            method: TLSv1_2
    """
    sub_profiles = {}
    for profile, ctx in profiles.get("tls", {}).items():
        sub_profiles[profile] = ctx
    return sub_profiles
